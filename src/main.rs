use std::collections::HashMap;


#[tokio::main]
// Example with rust version 1.50.0, not work with version > 1.42...
async fn main() -> Result<(), Box<dyn std::error::Error>> {
	let resp = reqwest::get("https://dantegraf.com/api/v1/health")
		.await?
		.json::<HashMap<String, String>>()
		.await?;

	match resp.get("status") {
		Some(status) => println!("Status is: {}", status),
		None => println!("Status not found")
	}

	Ok(())
}
